<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2009 Nigel McNie (http://nigel.mcnie.name/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-myforumposts
 * @author     Wullie Mair
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2011 Wullie Mair
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Mes messages des forums';
$string['description'] = 'Affiche mes messages publi�s dans des forums de groupes s�lectionn�s';

$string['group'] = 'Groupe';
$string['nogroupstochoosefrom'] = 'D�sol�, aucun message � afficher';
$string['poststoshow'] = 'Nombre maximum de messages � afficher pour chaque groupe';
$string['poststoshowdescription'] = 'Entre 1 et 100';
$string['recentforumpostsforgroup'] = "Derniers messages sur les forums de %s";
$string['defaulttitledescription'] = 'Un titre par d�faut sera cr�� si vous laissez cette rubrique vide';
$string['gotogroupforum'] = 'Aller aux forums de ce groupe';
$string['selectmultiplegroups'] = 'Appuyez sur Ctrl + clic sur chaque nom de groupe lorsque vous d�sirez s�lectionner plus d\'un groupe';
$string['noforumposts'] = 'Vous n\'avez aucun message dans ce forum, ou vous n\'avez pas la permission d\'acc�der � ce forum';
