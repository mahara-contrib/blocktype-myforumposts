<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2009 Nigel McNie (http://nigel.mcnie.name/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-myforumposts
 * @author     Wullie Mair
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2011 Wullie Mair
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'My Forum Posts';
$string['description'] = 'Display your forum posts for selected groups';

$string['group'] = 'Group';
$string['nogroupstochoosefrom'] = 'Sorry, no posts to show';
$string['poststoshow'] = 'Maximum number of posts to show for each group';
$string['poststoshowdescription'] = 'Between 1 and 100';
$string['recentforumpostsforgroup'] = "Recent Forum Posts for %s";
$string['defaulttitledescription'] = 'A default title will be generated if you leave the title field blank';
$string['gotogroupforum'] = 'Go to the forum for this group';
$string['selectmultiplegroups'] = 'Press Ctrl + click on each required group name to select more than one group';
$string['noforumposts'] = 'No posts exist within this forum, or you do not have permission to view any existing posts';
