<div id="recentforumpostsblock">
{if $postcount}
	{foreach from=$group key=groupcounter item=grouplist}	
		<div style="color:#6E8E00; font-weight:bold; font-size:16px; height:30px; display: table-cell; vertical-align:middle;">{$grouplist->name}</div>
		{assign var="posted" value="0"}
		<table class="fullwidth">
	      {foreach from=$foruminfo item=postinfo}
			{foreach from=$postinfo item=forumposts}
				{if $grouplist->id == $forumposts->group} 
					{assign var="posted" value="1"}
					<tr class="{cycle values='r0,r1'}">
						<td colspan="2">&nbsp;<strong>{$forumposts->title}</strong></td>
					</tr>
					<tr class="{cycle values='r0,r1'}">
     	      	    			<td width="80%"><strong><a href="{$WWWROOT}interaction/forum/topic.php?id={$forumposts->topic}#post{$forumposts->id}">&nbsp;&nbsp;&nbsp;&nbsp;{$forumposts->topicname}</strong></a><br /><div class="s">&nbsp;&nbsp;&nbsp;&nbsp;{$forumposts->body|str_shorten_html:100:true|safe}</div></td>
{*	      	      		<td width="20%" class="valign s center"><a href="{$WWWROOT}user/view.php?id={$forumposts->poster}"><img src="{$WWWROOT}thumb.php?type=profileicon&amp;maxsize=20&amp;id={$forumposts->poster}" alt=""><br />{$forumposts->poster|display_name}</a></td> *}
						<td width="20%">Posted: <br />{$forumposts->ctime}<br /></td>
      	      		</tr>
				{/if}
			{/foreach}
		{/foreach}	
		{if $posted == 1}
			<tr>
				<td colspan="3"><div class="morelinkwrap"><a class="morelink" href="{$WWWROOT}interaction/forum/?group={$grouplist->id}" target="_blank">{str tag=gotogroupforum section=blocktype.myforumposts} &raquo;</a></div><div class="cb"></div>
				</td>
			</tr>	
			</table>
				{if $groupcounter+1 < $groupcount} 
					<div><hr style="align:center; width:70%;" /></div>
				{/if}
		{else}
				<tr class="{cycle values='r0,r1'}">
 		     			<td align="center">{str tag=noforumposts section=blocktype.myforumposts}</td>
		      	</tr>
		      </table>
		{/if}
	{/foreach}
{else}
	<table class="fullwidth">
		<tr class="{cycle values='r0,r1'}">
        		<td align="center">{str tag=noforumposts section=blocktype.myforumposts}</td>
	      </tr>
      </table>
{/if}
</div>
